"""This is a program that reverses two strings and concatinates them"""

if __name__ == '__main__':
    str_1 = input("Enter first string: ")
    str_2 = input("Enter second string: ")

    output_string = str_1[::-1] + str_2[::-1]
    print(output_string)
